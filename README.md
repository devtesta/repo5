# Generated Documentation
## test_workflow
Description: Bla bla workflow test


| Field                | Value           |
|--------------------- |-----------------|
| Functional description | Not available. |
| Requester            | Not available. |
| Users                | Not available. |
| Date dev             | Not available. |
| Date prod            | Not available. |
| Readme update            | 02/10/2023 |
| Version              | Not available. |
| Time Saving              | Not available. |
| Category              | Not available. |
| Sub category              | Not available. |


### Defaults
**These are static variables with lower priority**
#### File: main.yml
| Var          | Type         | Value       | Required    | Title       |
|--------------|--------------|-------------|-------------|-------------|
| variable_test    | str   | test1  | True  | Variable test |
| variable_workflow    | str   | workflow1  | True  | Variable test |



### Vars

No vars available.


### Tasks
| Name | Module | Condition |
| ---- | ------ | --------- |
| Début du workflow | ansible.builtin.debug | False |
| Récupérer une variable dynamiquement (exemple avec un fichier) | ansible.builtin.slurp | False |
| Stocker la variable pour les étapes suivantes | ansible.builtin.set_fact | False |
| Block dans tasks | block | False |
| Afficher le contenu de la variable hostname si c'est une Debian | ansible.builtin.debug | True |
| Afficher le contenu de la variable variable_workflow si c'est une Debian | ansible.builtin.debug | True |
| Fin du workflow | ansible.builtin.debug | False |


## Task Flow Graphs

### Graph for main.yml
```mermaid
flowchart TD
  Start
  Start-->|Task| Début_du_workflow[début du workflow]
  Début_du_workflow-.->|End of Task| Début_du_workflow
  Début_du_workflow-->|Task| Récupérer_une_variable_dynamiquement__exemple_avec_un_fichier_[récupérer une variable dynamiquement  exemple avec<br>un fichier ]
  Récupérer_une_variable_dynamiquement__exemple_avec_un_fichier_-.->|End of Task| Récupérer_une_variable_dynamiquement__exemple_avec_un_fichier_
  Récupérer_une_variable_dynamiquement__exemple_avec_un_fichier_-->|Task| Stocker_la_variable_pour_les_étapes_suivantes[stocker la variable pour les étapes suivantes]
  Stocker_la_variable_pour_les_étapes_suivantes-.->|End of Task| Stocker_la_variable_pour_les_étapes_suivantes
  Stocker_la_variable_pour_les_étapes_suivantes-->|Block Start| Block_dans_tasks_block_start_0[block dans tasks]
  Block_dans_tasks_block_start_0-->|Task| Afficher_le_contenu_de_la_variable_hostname_si_c_est_une_Debian_when_ansible_os_family_____Debian_[afficher le contenu de la variable hostname si c<br>est une debian]
  Afficher_le_contenu_de_la_variable_hostname_si_c_est_une_Debian_when_ansible_os_family_____Debian_---|When: ansible os family     debian | Afficher_le_contenu_de_la_variable_hostname_si_c_est_une_Debian_when_ansible_os_family_____Debian_
  Afficher_le_contenu_de_la_variable_hostname_si_c_est_une_Debian_when_ansible_os_family_____Debian_-->|Task| Afficher_le_contenu_de_la_variable_variable_workflow_si_c_est_une_Debian_when_ansible_os_family_____Debian_[afficher le contenu de la variable variable<br>workflow si c est une debian]
  Afficher_le_contenu_de_la_variable_variable_workflow_si_c_est_une_Debian_when_ansible_os_family_____Debian_---|When: ansible os family     debian | Afficher_le_contenu_de_la_variable_variable_workflow_si_c_est_une_Debian_when_ansible_os_family_____Debian_
  Afficher_le_contenu_de_la_variable_variable_workflow_si_c_est_une_Debian_when_ansible_os_family_____Debian_-.->|End of Block| Block_dans_tasks_block_start_0
  Afficher_le_contenu_de_la_variable_variable_workflow_si_c_est_une_Debian_when_ansible_os_family_____Debian_-->|Rescue Start| Block_dans_tasks_rescue_start_0[block dans tasks]
  Block_dans_tasks_rescue_start_0-->|Task| Une_erreur_s_est_produite[une erreur s est produite]
  Une_erreur_s_est_produite-.->|End of Rescue Block| Block_dans_tasks_block_start_0
  Une_erreur_s_est_produite-->|Task| Fin_du_workflow[fin du workflow]
  Fin_du_workflow-.->|End of Task| Fin_du_workflow
```


## Playbook
```yml
---
- hosts: localhost
  remote_user: root
  roles:
    - test_workflow

```
## Playbook graph
```mermaid
flowchart TD
  localhost-->|Role| test_workflow[test workflow]
```

## Author Information
Lucian

#### License
license (GPL-2.0-or-later, MIT, etc)

#### Minimum Ansible Version
2.1

#### Platforms
No platforms specified.